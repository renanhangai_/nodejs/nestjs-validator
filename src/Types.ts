import JoiBase from "@hapi/joi";
import { StringSchema, NumberSchema, BooleanSchema, DateSchema, ArraySchema } from "@hapi/joi";
import { DecimalSchema } from "./validator/Decimal";
import { BigNumber } from "bignumber.js";

// Joi type
type JoiBaseType = typeof JoiBase;
export interface JoiType extends JoiBaseType {
	/**
	 * Validate against a decimal
	 */
	decimal(): DecimalSchema;
}

/// Schema Base
export type ValidatorSchemaBase = {
	[key: string]: any;
};

/// Validated object value
export type ValidatedValue<T> = T extends StringSchema
	? string
	: T extends NumberSchema
	? number
	: T extends BooleanSchema
	? boolean
	: T extends DateSchema
	? Date
	: T extends DecimalSchema
	? BigNumber
	: T extends ArraySchema
	? any[]
	: any;

/// Um objeto validado a partir de um validador genérico
export type ValidatedObject<T extends ValidatorSchemaBase> = {
	readonly [K in keyof T]: ValidatedValue<T[K]> | null
};

/// Validator context
export type ValidatorContext = {
	Joi: JoiType;
};
/// Options to validate the object
export type ValidateObjectOptions = {
	/// Allow unknown properties
	unknown?: boolean;
};
/// Schema for validator
export type ValidatorSchemaParam<T extends ValidatorSchemaBase> = (
	context: ValidatorContext
) => T | T;

declare module "@hapi/joi" {
	export interface DateSchema extends AnySchema {
		utc(): this;
	}
}
