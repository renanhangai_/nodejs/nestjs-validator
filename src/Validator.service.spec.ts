import { Test, TestingModule } from "@nestjs/testing";
import { ValidatorService } from "./Validator.service";
import BigNumber from "bignumber.js";
import { isSameDay } from "date-fns";

describe("ValidatorService", () => {
	let validatorService: ValidatorService;

	beforeAll(async () => {
		const app: TestingModule = await Test.createTestingModule({
			providers: [ValidatorService],
		}).compile();
		validatorService = app.get<ValidatorService>(ValidatorService);
	});

	describe("#validate (decimal)", () => {
		it("should describe decimal ", () => {
			validatorService.describe(({ Joi }) => Joi.decimal().format({}));
		});
		it("should validate decimal ", () => {
			const testDecimal = (input: any, check: any) => {
				const validatedOutput = validatorService.validate(input, ({ Joi }) => Joi.decimal());
				expect(validatedOutput).toBeInstanceOf(BigNumber);
				expect(validatedOutput.eq(check)).toBe(true);
			};
			const tests = [
				[null, new BigNumber(0)],
				[1000, new BigNumber("1000")],
				["100.00", new BigNumber("100.00")],
				[new BigNumber("100.00"), new BigNumber("100.00")],
			];
			for (const test of tests) {
				testDecimal(test[0], test[1]);
			}
		});
		it("should validate decimals with formats", () => {
			const testDecimal = (
				input: any,
				check: any,
				decimalSeparator?: string,
				thousandsSeparator?: string
			) => {
				const validatedOutput = validatorService.validate(input, ({ Joi }) =>
					Joi.decimal().format({
						decimalSeparator,
						thousandsSeparator,
					})
				);
				expect(validatedOutput).toBeInstanceOf(BigNumber);
				expect(validatedOutput.eq(check)).toBe(true);
			};
			const tests: any[] = [
				["100,00", new BigNumber("100.00"), ","],
				["1.123,00", new BigNumber("1123.00"), ",", "."],
				["1|123|123-99", new BigNumber("1123123.99"), "-", "|"],
			];
			for (const test of tests) {
				testDecimal(test[0], test[1], test[2], test[3]);
			}
		});

		it("should throw on invalid decimals", () => {
			const testInvalidDecimal = (
				input: any,
				decimalSeparator?: string,
				thousandsSeparator?: string
			) => {
				const fn = () => {
					validatorService.validate(input, ({ Joi }) =>
						Joi.decimal().format({
							decimalSeparator,
							thousandsSeparator,
						})
					);
				};
				expect(fn).toThrow();
			};
			const tests: any[] = [[NaN], [Infinity], [{}], ["Invalid Number"]];
			for (const test of tests) {
				testInvalidDecimal(test[0], test[1], test[2]);
			}
		});
	});

	describe("#validate (date)", () => {
		it("should describe date ", () => {
			validatorService.describe(({ Joi }) => Joi.date());
			validatorService.describe(({ Joi }) => Joi.date().utc());
			validatorService.describe(({ Joi }) => Joi.date().format("DD/MM/YYYY"));
		});

		it("should validate null", () => {
			const validatedOutput = validatorService.validate(null, ({ Joi }) =>
				Joi.date()
					.allow(null)
					.format("DD/MM/YYYY")
			);
			expect(validatedOutput).toBe(null);
		});
		it("should validate ISO", () => {
			const testValidInput = (input: string, date: Date) => {
				const validatedOutput = validatorService.validate(input, ({ Joi }) =>
					Joi.date()
						.required()
						.iso()
				);
				expect(validatedOutput).toBeInstanceOf(Date);
				expect(isSameDay(validatedOutput, date)).toBe(true);
			};
			testValidInput("1000-01-01", new Date(Date.UTC(1000, 0, 1)));
			testValidInput("9999-12-31", new Date(Date.UTC(9999, 11, 31)));
		});
		it("should validate date strings", () => {
			const testValidInput = (input: string, format: string | string[], date: Date) => {
				const validatedOutput = validatorService.validate(input, ({ Joi }) =>
					Joi.date()
						.required()
						.format(format)
				);
				expect(validatedOutput).toBeInstanceOf(Date);
				expect(isSameDay(validatedOutput, date)).toBe(true);
			};
			testValidInput("01/01/1000", "DD/MM/YYYY", new Date(1000, 0, 1));
			testValidInput("31/12/9999", "DD/MM/YYYY", new Date(9999, 11, 31));
			testValidInput("1000-01-01", "YYYY-MM-DD", new Date(1000, 0, 1));
			testValidInput("9999-12-31", "YYYY-MM-DD", new Date(9999, 11, 31));
			testValidInput("9999-12-31", ["DD/MM/YYYY", "YYYY-MM-DD"], new Date(9999, 11, 31));
		});

		it("should validate date objects", () => {
			const input = new Date(1900, 10, 20);
			const validatedOutput = validatorService.validate(input, ({ Joi }) =>
				Joi.date().format("DD/MM/YYYY")
			);
			expect(validatedOutput).toBeInstanceOf(Date);
			expect(isSameDay(validatedOutput, input)).toBe(true);
			expect(validatedOutput).not.toBe(input);
		});

		it("should validate UTF", () => {
			const input = new Date(1900, 10, 20);
			const validatedOutput = validatorService.validate(input, ({ Joi }) =>
				Joi.date()
					.utc()
					.format("DD/MM/YYYY")
			);
			expect(validatedOutput).toBeInstanceOf(Date);
			expect(isSameDay(validatedOutput, input)).toBe(true);
			expect(validatedOutput).not.toBe(input);
		});

		it("should throw on invalid date strings", async () => {
			const testInvalidInput = (input: string, format: string) => {
				const fn = () => {
					validatorService.validate(input, ({ Joi }) => Joi.date().format(format));
				};
				expect(fn).toThrow();
			};
			const invalidInputs = {
				"DD/MM/YYYY": ["I am batman", 0, 100, {}, "32/01/1999", "01/13/2013", "01/01/ABCD"],
			};
			for (const format in invalidInputs) {
				const inputs = invalidInputs[format];
				for (const invalidInput of inputs) {
					await testInvalidInput(invalidInput, format);
				}
			}
		});
	});

	describe("#validateObject", () => {
		it("should validate objects", () => {
			const obj = {
				number: 123,
				str: "My string",
				subobj: {
					number: 1.1,
					str: "My String",
					date: "10/10/2010",
				},
			};
			const expected = {
				number: 123,
				str: "My string",
				subobj: {
					number: 1.1,
					str: "My String",
					date: new Date(2010, 9, 10),
				},
			};
			const validatedObject = validatorService.validateObject(obj, ({ Joi }) => ({
				number: Joi.number(),
				str: Joi.string(),
				subobj: {
					number: Joi.number(),
					str: Joi.string(),
					date: Joi.date().format("DD/MM/YYYY"),
				},
			}));
			expect(validatedObject).toEqual(expected);
		});
	});
});
