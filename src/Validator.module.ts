import { Module, DynamicModule } from "@nestjs/common";
import { ValidatorService } from "./Validator.service";

export type ValidatorModuleOptions = {
	Joi?: any;
	useFactory?(): any;
};

/**
 *
 */
@Module({
	providers: [ValidatorService],
	exports: [ValidatorService],
})
export class ValidatorModule {
	/**
	 *
	 * @param options
	 */
	static forRoot(options?: ValidatorModuleOptions): DynamicModule {
		options = options || {};
		const providers = [];
		if (options.Joi) {
			providers.push({
				provide: ValidatorService.JOI_TOKEN,
				useValue: options.Joi,
			});
		} else if (options.useFactory) {
			providers.push({
				provide: ValidatorService.JOI_TOKEN,
				useFactory: options.useFactory,
			});
		}
		return {
			module: ValidatorModule,
			providers,
		};
	}
}
