import { Injectable, Inject, Optional } from "@nestjs/common";
import JoiBase from "@hapi/joi";
import { DecimalExtension } from "./validator/Decimal";
import { DateExtension } from "./validator/Date";
import {
	JoiType,
	ValidatorSchemaParam,
	ValidateObjectOptions,
	ValidatedObject,
	ValidatorContext,
	ValidatedValue,
} from "./Types";

/**
 * Validator service class
 */
@Injectable()
export class ValidatorService {
	public static readonly JOI_TOKEN = "VALIDATOR_SERVICE_JOI";

	/// Construct the joi
	private readonly Joi: JoiType;
	constructor(@Optional() @Inject(ValidatorService.JOI_TOKEN) joi: any) {
		if (!joi) joi = JoiBase;
		this.Joi = joi.extend([DecimalExtension, DateExtension]);
	}

	/// Valida o objeto
	validate<T>(
		data: any,
		schema: ValidatorSchemaParam<T>,
		options?: ValidateObjectOptions
	): ValidatedValue<T> {
		options = options || {};
		const normalizedSchema = this.normalizeSchema(schema);
		const allowUnknown = options.unknown !== false;
		const { error, value } = this.Joi.validate(data, normalizedSchema, {
			allowUnknown: allowUnknown,
			stripUnknown: allowUnknown,
		});
		if (error) throw error;
		return value;
	}

	/// Valida o objeto
	validateObject<T>(
		data: any,
		schema: ValidatorSchemaParam<T>,
		options?: ValidateObjectOptions
	): ValidatedObject<T> {
		let normalizedSchema = this.normalizeSchema(schema);
		if (!normalizedSchema.isJoi) {
			normalizedSchema = this.Joi.object().keys(normalizedSchema);
		}
		return this.validate(data, normalizedSchema, options);
	}

	/// Describe a schema
	describe<T>(schema: ValidatorSchemaParam<T>): JoiBase.Description {
		const normalizedSchema = this.normalizeSchema(schema);
		return this.Joi.describe(normalizedSchema);
	}

	/// Normalize the schema with the function
	private normalizeSchema<T>(schema: ValidatorSchemaParam<T>): any {
		if (typeof schema === "function") {
			const context: ValidatorContext = {
				Joi: this.Joi,
			};
			return schema(context);
		}
		return schema;
	}
}
