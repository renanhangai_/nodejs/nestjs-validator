import Joi from "@hapi/joi";
import { BigNumber } from "bignumber.js";
import escapeStringRegexp from "escape-string-regexp";

export type DecimalFormatOptions = {
	decimalSeparator?: string;
	thousandsSeparator?: string;
};

/**
 * Extend the validator to allow decimal types
 *
 * @param joi
 */
export const DecimalExtension = (joi: typeof Joi) => ({
	name: "decimal",
	language: {
		invalid: "must be a VALID number using the following format {{format}}",
		format: "must be a number using the following format {{format}}",
	},
	/// Convert the value
	coerce(value, state, options) {
		if (value == null || value === "") return new BigNumber(0);
		if (typeof value === "string") {
			const format = {
				decimalSeparator: ".",
				thousandsSeparator: "",
				...this._flags.format,
			};
			if (format.thousandsSeparator) {
				value = value.replace(new RegExp(escapeStringRegexp(format.thousandsSeparator), "g"), "");
			}
			if (format.decimalSeparator) {
				value = value.replace(format.decimalSeparator, ".");
			}
			const number = new BigNumber(value, 10);
			if (!number.isFinite())
				return this.createError(
					"decimal.invalid",
					{ value, format: this._flags.format },
					state,
					options
				);
			return number;
		} else if (typeof value === "number") {
			const number = new BigNumber(value, 10);
			if (!number.isFinite())
				return this.createError(
					"decimal.invalid",
					{ value, format: this._flags.format },
					state,
					options
				);
			return number;
		} else if (BigNumber.isBigNumber(value)) {
			return value;
		}
		return this.createError(
			"decimal.format",
			{ value, format: this._flags.format },
			state,
			options
		);
	},
	///
	rules: [
		{
			name: "format",
			description(params) {
				return `Decimal should respect format ${params.format}`;
			},
			params: {
				format: joi.object().keys({
					thousandsSeparator: joi.string(),
					decimalSeparator: joi.string(),
				}),
			},
			setup(params) {
				this._flags.format = params.format;
			},
			validate(params, value, state, options) {
				return value;
			},
		},
	],
});

export interface DecimalSchema extends Joi.AnySchema {
	format(options: DecimalFormatOptions): this;
}
