import Joi from "@hapi/joi";
import fecha from "fecha";

export const DateExtension = (joi: typeof Joi) => ({
	name: "date",
	base: joi.date(),
	language: {
		format: "must be a string with one of the following formats {{format}}",
	},
	/// Converte o valor
	coerce(value, state, options) {
		if (value == null) {
			return value;
		}
		if (value instanceof Date) {
			return new Date(value.getTime());
		}
		if (typeof value === "string") {
			if (this._flags.dateFormat && options.convert) {
				let date = tryParseDateFormats(value, this._flags.dateFormat);
				if (!date) {
					return this.createError(
						"date.format",
						{ value, format: this._flags.dateFormat },
						state,
						options
					);
				}
				return date;
			}
			return value;
		}
		return this.createError(
			"date.format",
			{ value, format: this._flags.dateFormat },
			state,
			options
		);
	},
	rules: [
		/// Novo método date().format(...)
		{
			name: "format",
			description(params) {
				return `Date should respect format ${params.format.join(" ")}`;
			},
			params: {
				format: joi
					.array()
					.items(joi.string())
					.single()
					.required(),
			},
			setup(params) {
				this._flags.dateFormat = params.format;
			},
			validate(params, value, state, prefs) {
				return value;
			},
		},
		/// Data deverá ser um utc
		{
			name: "utc",
			description(params) {
				return "Date should be interpreted in UTC";
			},
			setup(params) {
				this._flags.utc = true;
			},
			validate(params, value, state, prefs) {
				return value;
			},
		},
	],
});

function tryParseDateFormats(value: string, formats: string[]): Date {
	for (let i = 0, len = formats.length; i < len; ++i) {
		try {
			let date = fecha.parse(value, formats[i]);
			if (isNaN(date.getTime())) continue;
			const formatted = fecha.format(date, formats[i]);
			if (formatted !== value) continue;
			return date;
		} catch (e) {
			continue;
		}
	}
	return null;
}
